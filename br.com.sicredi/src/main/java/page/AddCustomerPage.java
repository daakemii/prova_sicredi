package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddCustomerPage {
	
	WebDriver driver; 
	
	
	@FindBy(id = "field-customerName")
	public WebElement INPUT_NAME;
	
	@FindBy(id = "field-contactLastName")
	public WebElement INPUT_LAST_NAME;
	
	@FindBy(id = "field-contactFirstName")
	public WebElement INPUT_CONTACT_FIRST_NAME;
	
	@FindBy(id = "field-phone")
	public WebElement INPUT_PHONE;
	
	@FindBy(id = "field-addressLine1")
	public WebElement INPUT_ADDRESS_LINE_ONE;
	
	@FindBy(id = "field-addressLine2")
	public WebElement INPUT_ADDRESS_LINE_TWO;
	
	@FindBy(id = "field-city")
	public WebElement INPUT_CITY;
	
	@FindBy(id = "field-state")
	public WebElement INPUT_STATE;
	
	@FindBy(id = "field-postalCode")
	public WebElement INPUT_POSTAL_CODE;
	
	@FindBy(id = "field-country")
	public WebElement INPUT_COUNTRY;

	@FindBy(id = "field_salesRepEmployeeNumber_chosen")
	public WebElement SELECT_EMPLOYEER;
	
	public WebElement SELECT_ITEM_EMPLOYEER(String employeer) {
		WebElement element;	
		String xpath = "//*[@id='field_salesRepEmployeeNumber_chosen']//*[@class='chosen-drop']//*[@class='chosen-results']//*[contains(text(),'"+employeer+"')]";
		element = driver.findElement(By.xpath(xpath));		
		return element;
	}
	
	@FindBy(id = "field-creditLimit")
	public WebElement INPUT_CREDIT_LIMIT;
	
	@FindBy(id = "form-button-save")
	public WebElement BTN_SAVE;
	
	@FindBy(id = "report-success")
	public WebElement MSG_SUCCESS;
	
	@FindBy(linkText = "Go back to list")
	public WebElement LINK_BACK_TO_LIST;
	
	public AddCustomerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	
	
	
}
