package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

	WebDriver driver; 
	
	
	@FindBy(id = "switch-version-select")
	public WebElement SELECT_SWITCH_VERSAO;
	
	@FindBy(xpath  = "//a[contains(.,'Add Customer')]")
	public WebElement BTN_ADD_CUSTOMER;
	
	@FindBy(css = "input[name='customerName']")
	public WebElement INPUT_CUSTOMER_NAME;	

	@FindBy(className = "select-all-none")
	public WebElement CHECKBOX_ITEM_SELECT;
	
	@FindBy(xpath = "//*[@id='gcrud-search-form']//*[@class='btn btn-default btn-outline-dark gc-refresh']")
	public WebElement BTN_REFRESH_CUSTOMER;
	
	@FindBy(xpath = "//*[@id='gcrud-search-form']//*[@class='btn btn-outline-dark delete-selected-button']")
	public WebElement BTN_DELETE_CUSTOMER;
	
	@FindBy(xpath = "//*[@class='delete-multiple-confirmation modal fade in show']//*[@class='btn btn-danger delete-multiple-confirmation-button']")
	public WebElement BTN_DELETE_CONFIRMATION;
	
	@FindBy(className = "alert-delete-multiple-one")
	public WebElement MSG_DELETE_CONFIRMATION;
	
	public By XpathLocator(String stringToLocator) {
		String xpath = "//*[contains(text(),'"+stringToLocator+"')]";
		return By.xpath(xpath);
	};
	
	public MainPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	
	
	
}
