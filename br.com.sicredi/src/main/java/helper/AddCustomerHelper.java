package helper;

import org.openqa.selenium.WebDriver;
import page.AddCustomerPage;
import util.SicrediUtil;

public class AddCustomerHelper extends SicrediUtil{

	AddCustomerPage addCustomerPage;
	
	public AddCustomerHelper(WebDriver driver) {
		super(driver);
		addCustomerPage = new AddCustomerPage(driver);
	}
	
	public void InputName(String name) {
		FillField(addCustomerPage.INPUT_NAME, name);
	}
	
	public void InputLastName(String lastName) {
		FillField(addCustomerPage.INPUT_LAST_NAME, lastName);
	}
	
	public void InputContactFirstName(String contactFirstName) {
		FillField(addCustomerPage.INPUT_CONTACT_FIRST_NAME, contactFirstName);
	}
	
	public void InputPhone(String phone) {
		FillField(addCustomerPage.INPUT_PHONE, phone);
	}
	
	public void InputAddressLineOne(String addressLineOne) {
		FillField(addCustomerPage.INPUT_ADDRESS_LINE_ONE, addressLineOne);
	}
	
	public void InputAddressLineTwo(String addressLineTwo) {
		FillField(addCustomerPage.INPUT_ADDRESS_LINE_TWO, addressLineTwo);
	}
	
	public void InputCity(String city) {
		FillField(addCustomerPage.INPUT_CITY, city);
	}
	
	public void InputState(String state) {
		FillField(addCustomerPage.INPUT_STATE, state);
	}
	
	public void InputPostalCode(String postalCode) {
		FillField(addCustomerPage.INPUT_POSTAL_CODE, postalCode);
	}
	
	public void InputCountry(String country) {
		FillField(addCustomerPage.INPUT_COUNTRY, country);
	}
	
	public void SelectEmployeer(String employeer) {
		ClickButton(addCustomerPage.SELECT_EMPLOYEER);
		
		ClickButton(addCustomerPage.SELECT_ITEM_EMPLOYEER(employeer));
	}
	
	public void InputCreditLimit(String creditLimit) {
		FillField(addCustomerPage.INPUT_CREDIT_LIMIT, creditLimit);
	}
	
	public void ClickSaveButton() {
		ClickButton(addCustomerPage.BTN_SAVE);
	}

	public String CheckMessageSuccess() {
		 return GetString(addCustomerPage.MSG_SUCCESS);
	}
	
	public void ClickBackToList() {
		ClickButton(addCustomerPage.LINK_BACK_TO_LIST);
	}
	
	
}
