package helper;

import org.openqa.selenium.WebDriver;

import page.MainPage;
import util.SicrediUtil;

public class MainHelper extends SicrediUtil{

	MainPage mainPage;
	
	public MainHelper(WebDriver driver) {
		super(driver);
		mainPage = new MainPage(driver);
	}
	
	public void ChangeSelectVersion(String text) {
		SelectItemByText(mainPage.SELECT_SWITCH_VERSAO, text);
	}
	
	public void ClickAddCustomer() {
		ClickButton(mainPage.BTN_ADD_CUSTOMER);
	}
	
	public void InputSearchName(String name) {
		FillField(mainPage.INPUT_CUSTOMER_NAME, name);
	}
	
	public void CheckSearchCountryResult(String country) {
		WaitUntilElementVisibility(WaitElementBePresent(mainPage.XpathLocator(country)));
	}
	
	public void ClickRefreshCustomer() {
		ClickButton(mainPage.BTN_REFRESH_CUSTOMER);
	}
	
	public void CheckboxItemSelect() {
		ClickButton(mainPage.CHECKBOX_ITEM_SELECT);
	}
	
	public void ClickDeleteCustomer() {
		ClickButton(mainPage.BTN_DELETE_CUSTOMER);
	}
	
	public void ClickDeleteConfirmation() {
		ClickButton(mainPage.BTN_DELETE_CONFIRMATION);
	}
	
	public String CheckMessageDeleteConfirmation() {
		 return GetString(mainPage.MSG_DELETE_CONFIRMATION);
	}
	
	public String CheckMessageDeleteSuccess(String msg) {
		 return GetString(WaitElementBePresent(mainPage.XpathLocator(msg)));
	}
	
}
