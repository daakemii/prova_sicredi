package util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SicrediUtil {
	
	WebDriver driver;
	WebDriverWait wait;
	
	private final long TIME_WAIT_IN_SECONDS = 20; 
	
	public SicrediUtil(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, TIME_WAIT_IN_SECONDS);
	}

	protected void FillField(WebElement element, String text) {
		wait.until(ExpectedConditions.visibilityOf(element));
		element.click();
		element.sendKeys(text);
	}
	
	protected void SelectItemByText(WebElement element, String text) {
		wait.until(ExpectedConditions.visibilityOf(element));
		Select select = new Select(element);
		select.selectByVisibleText(text);
	}
		
	protected void ClickButton(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	protected String GetString(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		return element.getText();
	}
	
	protected WebElement WaitElementBePresent(By locator) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	
	protected void WaitUntilElementVisibility(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
}
