package br.com.sicredi;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import helper.AddCustomerHelper;
import helper.MainHelper;


public class TestSicredi {

	 static String driverPath = "C:\\chromedriver.exe";
	
	 static WebDriver driver;
	
	 static MainHelper mainHelper;
	 static AddCustomerHelper acHelper;
	 
	 //
	 String name = "Teste Sicredi";
	 String contry = "Brasil";
	
	 @BeforeClass
	 public static void StartService() {
		 System.setProperty("webdriver.chrome.driver", driverPath);
		 driver = new ChromeDriver();
		 driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");
		 driver.manage().window().maximize();
		 mainHelper = new MainHelper(driver);
		 acHelper = new AddCustomerHelper(driver);
	 }
	 	 
	 @Test
	 public void challenge01() {

		 
		 mainHelper.ChangeSelectVersion("Bootstrap V4 Theme");
		 mainHelper.ClickAddCustomer();
		 
		 acHelper.InputName(name);
		 acHelper.InputLastName("Teste");
		 acHelper.InputContactFirstName("seu nome");
		 acHelper.InputPhone("51 9999-9999");
		 acHelper.InputAddressLineOne("Av Assis Brasil, 3970");
		 acHelper.InputAddressLineTwo("Torre D");
		 acHelper.InputCity("Porto Alegre");
		 acHelper.InputState("RS");
		 acHelper.InputPostalCode("91000-000");
		 acHelper.InputCountry(contry);
		 acHelper.SelectEmployeer("Fixter");
		 acHelper.InputCreditLimit("200");
		 acHelper.ClickSaveButton();
		 assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list", acHelper.CheckMessageSuccess());	 
	 }
	 
	 @Test
	 public void Challenge02() {
		 
		 String msgCustomerDeletedSuccess = "Your data has been successfully deleted from the database.";
		 
		 acHelper.ClickBackToList();
		 
		 mainHelper.InputSearchName(name);
		 mainHelper.CheckSearchCountryResult(contry);
		 mainHelper.ClickRefreshCustomer();
		 mainHelper.CheckboxItemSelect();
		 mainHelper.ClickDeleteCustomer();
		 
		 assertEquals("Are you sure that you want to delete this 1 item?", mainHelper.CheckMessageDeleteConfirmation());	 
		
		 mainHelper.ClickDeleteConfirmation();
		 
		 assertEquals(msgCustomerDeletedSuccess, mainHelper.CheckMessageDeleteSuccess(msgCustomerDeletedSuccess));	
	 }
	 
	
	 @AfterClass
	 public static void CloseService() {
		 driver.close();
	 }
	
	
}
